﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using System.Text.RegularExpressions;

namespace PlayerRanker
{
	using YearToStatLinesDict = System.Collections.Generic.Dictionary<int, PlayerSeasonStatLine>;
	//	using YearToStatDict = System.Collections.Generic.Dictionary<int, double>;
	//    using PlayerToYears = System.Collections.Generic.Dictionary<PlayerName, System.Collections.Generic.Dictionary<int, double>>;
	//using PlayerToAllStats = System.Collections.Generic.Dictionary<PlayerName, PlayerAllStats>;

	static class Constants
	{
		static public readonly int CurrentYear = 2017;
		static public readonly BasicStatPredictor GPGPredictor = new BasicStatPredictor(new double[] { 2.08, -32.2, 128.57 }, new double[] { });
		static public readonly double[] PreviousGPGWeights = { 100, 70, 50, 35, 20, 10, 5 };
		static public readonly string FileNameStarts = "TestAutodraft";
		static public readonly string ExpectedFileNameOutput = "ExpectedStats.csv";
		static public readonly string AggregateFileName = "AggregateData.csv";
	}

	#region CSV Classes
	[DelimitedRecord(",")]
	public class PlayerSeasonStatLine
	{
		public string Name;
		public string FirstName;
		public string Position;
		public string Team;
		public int GamesPlayed;
		public int Goals;
		public int Assists;
		public int Points;
		public double PointsPerGame;
		public int PlusMinus;
		public int PenaltyMinutes;
		public int Hits;
		public int BlockedShots;
		public int PowerplayGoals;
		public int PowerplayAssists;
		public int ShorthandedGoals;
		public int ShorthandedAssists;
		public int GameWinningGoals;
		public int ShotsOnGoal;
		public double ShotPercentage;
	}

	[DelimitedRecord(",")]
	public class PlayerSeasonExpectedStatLine
	{
		public string Name;
		public string FirstName;
		public double GoalsPerGame;
		public double GoalsPerGameError;
		public double AssistsPerGame;
		public double AssistsPerGameError;
		public double PointsPerGame;
		public double PointsPerGameError;
	}

	[DelimitedRecord(",")]
	public class AggregateStatProperties
	{
		public string Year;
		public string AverageGPGError;
		public string AverageAPGError;
		public string AveragePPGError;
	}
	#endregion


	public class YearDict<TValue> : Dictionary<int, TValue>
	{
		public void SafeAdd(int year, TValue value)
		{
			TValue outValue;
			if (!TryGetValue(year, out outValue))
			{
				Add(year, value);
			}
			else
				outValue = value;
		}
	}

	public class YearsToStat : YearDict<double>
	{
	}

	public class PlayerDict<TValue> : Dictionary<PlayerName, TValue>
	{
		public PlayerDict()
			: base(new NameEqualityComparer())
		{
		}
	}
	public class PlayersToYearDict<TValue> : PlayerDict<YearDict<TValue>>
	{

		public void SafeAdd(PlayerName playerName, int year, TValue value)
		{
			YearDict<TValue> yearDict;
			if (!TryGetValue(playerName, out yearDict))
			{
				yearDict = new YearDict<TValue>();
				Add(playerName, yearDict);
			}

			yearDict.SafeAdd(year, value);
		}
	}

	public class PlayersToYearsToStat : PlayersToYearDict<double>
	{
	}

	//change to Stat -> Player -> year ->Value
	public class AllStats
	{
		public AllStats()
		{
			Teams = new PlayersToYearDict<string>();
			GamesPlayed = new PlayersToYearsToStat();
			Goals = new PlayersToYearsToStat();
			GoalsPerGame = new PlayersToYearsToStat();
			Assists = new PlayersToYearsToStat();
			AssistsPerGame = new PlayersToYearsToStat();
			Points = new PlayersToYearsToStat();
			PointsPerGame = new PlayersToYearsToStat();
			PlusMinus = new PlayersToYearsToStat();
			PenaltyMinutes = new PlayersToYearsToStat();
			Hits = new PlayersToYearsToStat();
			BlockedShots = new PlayersToYearsToStat();
			PowerplayGoals = new PlayersToYearsToStat();
			PowerplayAssists = new PlayersToYearsToStat();
			ShorthandedGoals = new PlayersToYearsToStat();
			ShorthandedAssists = new PlayersToYearsToStat();
			GameWinningGoals = new PlayersToYearsToStat();
			ShotsOnGoal = new PlayersToYearsToStat();
			ShotPercentage = new PlayersToYearsToStat();
		}

		public void AddStatLine(PlayerName playerName, int year, PlayerSeasonStatLine statLine)
		{
			Teams.SafeAdd(playerName, year, statLine.Team);
			GamesPlayed.SafeAdd(playerName, year, statLine.GamesPlayed);
			Goals.SafeAdd(playerName, year, statLine.Goals);
			GoalsPerGame.SafeAdd(playerName, year, (double)statLine.Goals / (double)statLine.GamesPlayed);
			Assists.SafeAdd(playerName, year, statLine.Assists);
			AssistsPerGame.SafeAdd(playerName, year, (double)statLine.Assists / (double)statLine.GamesPlayed);
			Points.SafeAdd(playerName, year, statLine.Assists);
			PointsPerGame.SafeAdd(playerName, year, (double)statLine.Assists / (double)statLine.GamesPlayed);
			PlusMinus.SafeAdd(playerName, year, statLine.PlusMinus);
			PenaltyMinutes.SafeAdd(playerName, year, statLine.PenaltyMinutes);
			Hits.SafeAdd(playerName, year, statLine.Hits);
			BlockedShots.SafeAdd(playerName, year, statLine.BlockedShots);
			PowerplayGoals.SafeAdd(playerName, year, statLine.PowerplayGoals);
			PowerplayAssists.SafeAdd(playerName, year, statLine.PowerplayAssists);
			ShorthandedGoals.SafeAdd(playerName, year, statLine.ShorthandedGoals);
			ShorthandedAssists.SafeAdd(playerName, year, statLine.ShorthandedAssists);
			GameWinningGoals.SafeAdd(playerName, year, statLine.GameWinningGoals);
			ShotsOnGoal.SafeAdd(playerName, year, statLine.ShotsOnGoal);
			ShotPercentage.SafeAdd(playerName, year, statLine.ShotPercentage);
		}


		//TODO change all to double and use typedef
		public PlayersToYearDict<string> Teams;
		public PlayersToYearsToStat GamesPlayed;
		public PlayersToYearsToStat Goals;
		public PlayersToYearsToStat GoalsPerGame;
		public PlayersToYearsToStat Assists;
		public PlayersToYearsToStat AssistsPerGame;
		public PlayersToYearsToStat Points;
		public PlayersToYearsToStat PointsPerGame;
		public PlayersToYearsToStat PlusMinus;
		public PlayersToYearsToStat PenaltyMinutes;
		public PlayersToYearsToStat Hits;
		public PlayersToYearsToStat BlockedShots;
		public PlayersToYearsToStat PowerplayGoals;
		public PlayersToYearsToStat PowerplayAssists;
		public PlayersToYearsToStat ShorthandedGoals;
		public PlayersToYearsToStat ShorthandedAssists;
		public PlayersToYearsToStat GameWinningGoals;
		public PlayersToYearsToStat ShotsOnGoal;
		public PlayersToYearsToStat ShotPercentage;

// 		public Dictionary<string,>
	}

	public class PlayerSeasonExpectedStatInfo
	{
		public PlayerSeasonExpectedStatInfo()
		{
			Value = 0;
			Error = -1;
		}

		public double Value;
		public double Error;
	}

	public class PlayerName
	{
		public PlayerName(string name, string firstName)
		{
			Name = name;
			FirstName = firstName;
		}

		public string Name;
		public string FirstName;
	}

	public class NameEqualityComparer : IEqualityComparer<PlayerName>
	{
		public bool Equals(PlayerName p1, PlayerName p2)
		{
			if (p2 == null && p1 == null)
				return true;
			else if (p2 == null | p1 == null)
				return false;
			else if (p2.Name == p1.Name && p2.FirstName == p1.FirstName)
				return true;
			else
				return false;
		}

		public int GetHashCode(PlayerName playerName)
		{
			string hCode = playerName.Name + playerName.FirstName;
			return hCode.GetHashCode();
		}
	}

	class PlayerRanker
	{
		static void Main(string[] args)
		{
			PlayerRanker playerRanker = new PlayerRanker();

			//Hold every year's stats for every player using their fullname as key
			AllStats allStats = new AllStats();
			//PlayerToAllStats playersToAllStats = new Dictionary<PlayerName, PlayerAllStats>(new NameEqualityComparer());

			//Delete the current Aggregate file
			File.Delete(Constants.AggregateFileName);

			//Write Headers
			AggregateStatProperties aggregateStatProperties = new AggregateStatProperties();
			aggregateStatProperties.Year = "Year";
			aggregateStatProperties.AverageGPGError = "Average GPG Error";
			aggregateStatProperties.AverageAPGError = "Average APG Error";
			aggregateStatProperties.AveragePPGError = "Average PPG Error";
			var aggregateWriter = new FileHelperEngine<AggregateStatProperties>();
			aggregateWriter.AppendToFile(Constants.AggregateFileName, aggregateStatProperties);

			//////////////////////////////////////////////
			// Read existing player stats and store them
			int numYears = playerRanker.ReadInputYears(allStats);

			PredictionParamsCalculator.CalculateParams(allStats.GoalsPerGame);

			//TODO add Predcitor calculation algorithm call(s) and write to file?
			//Or have separate project for algorithm which writes to file and we just read values from file here

			//////////////////////////////////////////////
			// Calculate Expected stats for the following year and write them out
			playerRanker.WriteExpectedYear(allStats, Constants.CurrentYear, Constants.ExpectedFileNameOutput);

			//////////////////////////////////////////////
			// Calculate Expected stats for previous years, and errors from actual stats, and write them out
			playerRanker.WriteExpectedPrevious(allStats, numYears);
		}

		public int ReadInputYears(AllStats allStats)
		{
			DirectoryInfo currentDirectory = new DirectoryInfo(Environment.CurrentDirectory);
			FileInfo[] inputFiles = currentDirectory.GetFiles(Constants.FileNameStarts + "*.csv");

			var engineReader = new FileHelperEngine<PlayerSeasonStatLine>();

			//Read every file passed in arguments
			foreach (FileInfo seasonStatsFile in inputFiles)
			{
				var seasonStatLines = engineReader.ReadFile(seasonStatsFile.Name);

				//Extract year from file name
				int year = Int32.Parse(Regex.Replace(seasonStatsFile.Name, @"[^\d]+", ""));

				//Read every player's statline from that year and add them to the player dictionary
				foreach (PlayerSeasonStatLine statLine in seasonStatLines)
				{
					PlayerName playerName = new PlayerName(statLine.Name, statLine.FirstName);
					allStats.AddStatLine(playerName, year, statLine);
				}
			}

			return inputFiles.Count();
		}

		static public PlayerSeasonExpectedStatInfo calculateExpectedPlayerStatForYear(YearDict<double> yearsToStat, BasicStatPredictor gpgPredictor, int yearToCalculate)
		{
			var expectedStatInfo = new PlayerSeasonExpectedStatInfo();
			double totalUsedWeight = 0;
			bool hasPreviousStats = false;

			foreach (var statInfo in yearsToStat)
			{
				int year = statInfo.Key;
				double statValue = statInfo.Value;
				int yearDiff = yearToCalculate - year;

				if (yearDiff > 0)
				{
					double weight = 0;
					int numParams = gpgPredictor.PrevStatsPoly.Count();

					for (int i = 0; i < numParams; i++)
					{
						int paramOrder = numParams - 1 - i;
						weight += gpgPredictor.PrevStatsPoly[i] * Math.Pow(yearDiff, paramOrder);
					}

					//TODO add trend calculation

					expectedStatInfo.Value += statValue * weight;
					totalUsedWeight += weight;
					hasPreviousStats = true;
				}
			}

			//Normalize expected PPG
			if (totalUsedWeight > 0)
				expectedStatInfo.Value *= 1 / totalUsedWeight;

			if (hasPreviousStats && yearsToStat.ContainsKey(yearToCalculate))
			{
				//TODO: do we calculate error in relation to actual value? What is the error then when actual value is 0? Infinity fucks shit up
				//If both actual value and expected value are 0, then error should be 0
				Double statValue;
				if (yearsToStat.TryGetValue(yearToCalculate, out statValue))
				{
					if (expectedStatInfo.Value > 0.00001)
						expectedStatInfo.Error = Math.Abs(expectedStatInfo.Value - statValue) / expectedStatInfo.Value;
					else if (expectedStatInfo.Value == 0 && statValue == 0)
						expectedStatInfo.Error = 0;
				}
			}

			return expectedStatInfo;
		}

		private List<PlayerSeasonExpectedStatLine> getExpectedStatLinesForYear(AllStats allStats, int yearToCalculate)
		{
			PlayerDict<PlayerSeasonExpectedStatLine> expectedPlayersToStatLines = new PlayerDict<PlayerSeasonExpectedStatLine>();

			BasicStatPredictor gpgPredictor = Constants.GPGPredictor; //TODO get predictor from file?

			//Goals Per GAme
			foreach (var playersYearsToGPG in allStats.GoalsPerGame)
			{
				PlayerName playerName = playersYearsToGPG.Key;
				YearDict<double> yearsToStat = playersYearsToGPG.Value;
				PlayerSeasonExpectedStatInfo expectedStatInfo = calculateExpectedPlayerStatForYear(yearsToStat, gpgPredictor, yearToCalculate);

				PlayerSeasonExpectedStatLine expectedStatLine = new PlayerSeasonExpectedStatLine();
				expectedStatLine.GoalsPerGameError = expectedStatInfo.Error;
				expectedStatLine.GoalsPerGame = expectedStatInfo.Value;
				expectedStatLine.FirstName = playerName.FirstName;
				expectedStatLine.Name = playerName.Name;

				expectedPlayersToStatLines.Add(playerName, expectedStatLine);
			}

			//Points Per Game
			foreach (var playersYearsToPPG in allStats.PointsPerGame)
			{
				PlayerName playerName = playersYearsToPPG.Key;
				YearDict<double> yearsToStat = playersYearsToPPG.Value;
				PlayerSeasonExpectedStatInfo expectedStatInfo = calculateExpectedPlayerStatForYear(yearsToStat, gpgPredictor, yearToCalculate);

				PlayerSeasonExpectedStatLine expectedStatLine;

				if (expectedPlayersToStatLines.TryGetValue(playerName, out expectedStatLine))
				{
					expectedStatLine.PointsPerGameError = expectedStatInfo.Error;
					expectedStatLine.PointsPerGame = expectedStatInfo.Value;
				}			   
			}

			//Add other stats here

			return expectedPlayersToStatLines.Values.ToList();
		}

		public void WriteExpectedYear(AllStats allStats, int yearToCalculate, string outputFileName) 
		{
			List<PlayerSeasonExpectedStatLine> expectedStatLines = getExpectedStatLinesForYear(allStats, yearToCalculate);

			if (yearToCalculate != Constants.CurrentYear)
			{
				AggregateStatProperties aggregateStatProperties = new AggregateStatProperties();
				aggregateStatProperties.Year = yearToCalculate.ToString();

				double averageGPGError = 0;
				int numGPGCounted = 0;
				double averagePPGError = 0;
				int numPPGCounted = 0;
				foreach (PlayerSeasonExpectedStatLine statLine in expectedStatLines)
				{
					if (statLine.GoalsPerGameError > 0)
					{
						averageGPGError += statLine.GoalsPerGameError;
						++numGPGCounted;
					}
					if (statLine.PointsPerGameError > 0)
					{
						averagePPGError += statLine.PointsPerGameError;
						++numPPGCounted;
					}
				}

				if (numGPGCounted > 0)
					averageGPGError /= numGPGCounted;

				if (numPPGCounted > 0)
					averagePPGError /= numPPGCounted;

				aggregateStatProperties.AverageGPGError = averageGPGError.ToString("#0.##%");
				aggregateStatProperties.AveragePPGError = averagePPGError.ToString("#0.##%");


				var aggregateWriter = new FileHelperEngine<AggregateStatProperties>();
				aggregateWriter.AppendToFile(Constants.AggregateFileName, aggregateStatProperties);

			}

			var engineWriter = new FileHelperEngine<PlayerSeasonExpectedStatLine>();
			engineWriter.WriteFile(outputFileName, expectedStatLines);
		}

		public void WriteExpectedPrevious(AllStats allStats, int numYears)
		{
			for (int i = 1; i < numYears; ++i) {
				int year = Constants.CurrentYear - i;
				WriteExpectedYear(allStats, year, "Previous(" + year + ")" + Constants.ExpectedFileNameOutput);
			}
		}
	}
}
