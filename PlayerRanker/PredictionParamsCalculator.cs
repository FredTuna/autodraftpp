﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Populations;

namespace PlayerRanker
{
	using System.IO;
	using FileHelpers;
	using GeneticSharp.Domain.Crossovers;
	using GeneticSharp.Domain.Mutations;
	using GeneticSharp.Domain.Selections;
	using GeneticSharp.Domain.Terminations;
	using YearToStatsDict = System.Collections.Generic.Dictionary<int, double>;

	static class GAConstants
	{
		static public double crossoverProbability = 0.65;
		static public double mutationProbability = 0.08;
		static public int elitismPercentage = 5;

		static public int PolyOrder = 2;

		static public float minPolyParam = -1000f;
		static public float maxPolyParam = 1000f;

		static public string GenerationsResultsFilename = "GenerationsResults.csv";
	}

	[DelimitedRecord(",")]
	public class GenerationInfo
	{
		public string GenerationNumber;
		public string BestWeights;
		public string TotalError;

	}

	class BasicStatPredictor
	{
		public BasicStatPredictor(int polyCount)
		{
			PrevStatsPoly = new double[polyCount];
			StatsTrendPoly = new double[polyCount];
		}

		public BasicStatPredictor(double[] prevStatsPoly, double[] statsTrendPoly)
		{
			PrevStatsPoly = prevStatsPoly;
			StatsTrendPoly = statsTrendPoly;
		}

		public double[] PrevStatsPoly;
		public double[] StatsTrendPoly;
	}

	class BasicStatsHelper
	{
		public BasicStatsHelper(int polyOrder)
		{
			PolyOrder = polyOrder;
		}

		static public FloatingPointChromosome GetChromosome()
		{
			int numValues = GetTotalNumValues();
			int bitsNeeded = Convert.ToInt32(Math.Floor(Math.Log(GAConstants.maxPolyParam - GAConstants.minPolyParam, 2) + 1));

			double[] minValues = new double[numValues];
			double[] maxValues = new double[numValues];
			int[] totalBits = new int[numValues];
			int[] fractionBits = new int[numValues];

			for (int i = 0; i < numValues; i++)
			{
				minValues[i] = GAConstants.minPolyParam;
				maxValues[i] = GAConstants.maxPolyParam;
				totalBits[i] = 64;
				fractionBits[i] = 5;
			}

			return new FloatingPointChromosome(minValues, maxValues, totalBits, fractionBits);
		}

		static public BasicStatPredictor GetPredictorFromValues(double[] chromosomeValues)
		{
			int numValuesPerPoly = GetNumValuesPerPoly();
			BasicStatPredictor basicStatPredictor = new BasicStatPredictor(numValuesPerPoly);

			for (int i = 0; i < numValuesPerPoly; i++)
			{
				basicStatPredictor.PrevStatsPoly[i] = chromosomeValues[i];
				basicStatPredictor.StatsTrendPoly[i] = chromosomeValues[i + numValuesPerPoly];
			}

			return basicStatPredictor;
		}

		static public double EvaluateChromosomeValues(double[] chromosomeValues, PlayersToYearsToStat playersToYearToStats)
		{
			double error = 0;
			double numStatsCounted = 0;
			BasicStatPredictor basicStatPredictor = GetPredictorFromValues(chromosomeValues);

			foreach (var playerToYearsToStat in playersToYearToStats)
			{
				var yearsToStat = playerToYearsToStat.Value;
				foreach (var yearToStat in yearsToStat)
				{
					PlayerSeasonExpectedStatInfo expectedStatInfo = PlayerRanker.calculateExpectedPlayerStatForYear(yearsToStat, basicStatPredictor, yearToStat.Key);
					error += expectedStatInfo.Error;
					numStatsCounted++;
				}
			}

			return error / numStatsCounted;
		}

		static private int GetTotalNumValues()
		{
			return (PolyOrder + 1) * NumPolys;
		}

		static private int GetNumValuesPerPoly()
		{
			return (PolyOrder + 1);
		}

		static private int PolyOrder;
		static private int NumPolys = 2;
	}

// 	class BasicStatPredictorChromosome : FloatingPointChromosome, BasicStatPredictor
// 	{
// 		public BasicStatPredictorChromosome(double[] minValue, double[] maxValue, int[] totalBits, int[] fractionBits)
// 			: base(minValue, maxValue, totalBits, fractionBits)
// 		{
// 			int numValuesPerPoly = getNumValuesPerPoly();
// 			double[] prevStatsPoly = new double[numValuesPerPoly];
// 			double[] statsTrendPoly = new double[numValuesPerPoly];
// 
// 
// 			for (int i = 0; i < numValuesPerPoly; i++)
// 			{
// 				prevStatsPoly[i] = chromosomeValues[i];
// 				statsTrendPoly[i] = chromosomeValues[i + numValuesPerPoly];
// 			}
// 			statPredictor.PrevStatsPoly
// 		}
// 
// 		public BasicStatPredictor statPredictor;
// 	}

	class BasicStatsPredictorFitness : IFitness
	{
		public BasicStatsPredictorFitness(PlayersToYearsToStat playerToYearStat)
		{
			PlayerToYearStat = playerToYearStat;
		}

		public double Evaluate(IChromosome chromosome)
		{
			var basicStatsChromosome = chromosome as FloatingPointChromosome;
			var values = basicStatsChromosome.ToFloatingPoints();

			return BasicStatsHelper.EvaluateChromosomeValues(values, PlayerToYearStat);
		}

		PlayersToYearsToStat PlayerToYearStat;
	}

	class PredictionParamsCalculator
	{
		public PredictionParamsCalculator()
		{
		}
		static public void CalculateParams(PlayersToYearsToStat playerToYearStat)
		{

			BasicStatsHelper helper = new BasicStatsHelper(GAConstants.PolyOrder);

			//TODO : new chromosome class with fitness for several years
			var chromosome = BasicStatsHelper.GetChromosome();
			var population = new Population(10, 100, chromosome);			
			var fitness = new BasicStatsPredictorFitness(playerToYearStat);
			var selection = new EliteSelection();
			var crossover = new UniformCrossover(0.5f);
			var mutation = new FlipBitMutation();
			var termination = new FitnessStagnationTermination(10);

			var ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation);

			ga.Termination = termination;

			const int pastYearsToShow = 7;
			GenerationInfo generationInfoHeader = new GenerationInfo();
			generationInfoHeader.GenerationNumber = "Generation Number";
			generationInfoHeader.BestWeights = "Best Weights";
			generationInfoHeader.TotalError = "Total Error";

			for (int i = 1; i < pastYearsToShow; i++)
				generationInfoHeader.BestWeights += ", ";

			File.Delete(GAConstants.GenerationsResultsFilename);
			var generationsWriter = new FileHelperEngine<GenerationInfo>();
			generationsWriter.AppendToFile(GAConstants.GenerationsResultsFilename, generationInfoHeader);


			List<GenerationInfo> generationsInfos = new List<GenerationInfo>();

			var latestFitness = 0.0;
			ga.GenerationRan += (sender, e) =>
			{
				var bestChromosome = ga.BestChromosome as FloatingPointChromosome;
				var bestFitness = bestChromosome.Fitness.Value;

				if (bestFitness != latestFitness)
				{
					BasicStatPredictor basicStatPredictor = BasicStatsHelper.GetPredictorFromValues(bestChromosome.ToFloatingPoints());
					String weights = "";



					for (int i = 1; i < pastYearsToShow; i++)
					{
						double weight = 0;
						int numParams = basicStatPredictor.PrevStatsPoly.Count();

						for (int j = 0; j < numParams; j++)
						{
							int paramOrder = numParams - 1 - j;
							weight += basicStatPredictor.PrevStatsPoly[j] * Math.Pow(i, paramOrder);
						}

						weights += weight.ToString("#0.##");
						
						weights += ", ";
					}

					GenerationInfo generationInfo = new GenerationInfo();
					generationInfo.GenerationNumber = ga.GenerationsNumber.ToString();
					generationInfo.TotalError = bestFitness.ToString("#0.##%");
					generationInfo.BestWeights = weights;

					generationsInfos.Add(generationInfo);
				}
			};


			ga.Start();

			generationsWriter.AppendToFile(GAConstants.GenerationsResultsFilename, generationsInfos);

			Console.ReadKey();
		}
	}
}