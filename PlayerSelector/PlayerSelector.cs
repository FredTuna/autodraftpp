﻿using FileHelpers;
using PlayerRanker;
using System;
using System.IO;
using System.Collections.Generic;

namespace PlayerSelector
{
    class PlayerSelector
    {
        static class Constants
		{
			static public readonly int Year = 2016;
			static public readonly string InputPlayerStats = "TestAutodraft++ - " + (Year - 1) + ".csv";
			static public readonly string InputDraftOrder = "YahooDraftOrder" + Year + ".csv";
			static public readonly string OutputDraftOrder = "PlayerSelectionOrder" + Year + ".csv";
			static public readonly string OutputAggregateStats = "PlayerSelectionOrder" + Year + ".csv";
        }

        public static void Main(string[] args)
		{
            //Hold every year's stats for every player using their fullname as key
            Dictionary<PlayerName, PlayerSeasonStatLine> playersToStatLines = ReadInputPlayerStats();


		}

		static Dictionary<PlayerName, PlayerSeasonStatLine> ReadInputPlayerStats()
		{
            Dictionary<PlayerName, PlayerSeasonStatLine> playersToStatLines = new Dictionary<PlayerName, PlayerSeasonStatLine>(new NameEqualityComparer());
			FileHelperEngine<PlayerSeasonStatLine> engineReader = new FileHelperEngine<PlayerSeasonStatLine>();
            PlayerSeasonStatLine[] seasonStatLines = engineReader.ReadFile(Constants.InputPlayerStats);

			//Read every player's statline from that year and add them to the player dictionary
			foreach (PlayerSeasonStatLine statLine in seasonStatLines)
			{
				PlayerName playerName = new PlayerName(statLine.Name, statLine.FirstName);
				playersToStatLines.Add(playerName, statLine);
			}

            return playersToStatLines;
		}
    }
}
