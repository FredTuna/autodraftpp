﻿using PlayerRanker;
using System.Collections.Generic;

namespace PlayerSelector
{
    enum SelectionCriteriaOptions 
    {
        Goals,
        Assists,
        Points,
        PlusMinus,
        PenaltyMinutes,
        Hits,
        BlockedShots,
        PowerplayGoals,
        PowerplayAssists,
        ShorthandedGoals,
        ShorthandedAssists,
        GameWinningGoals,
        ShotsOnGoal,
        Count
    }

    interface ISelectionStrategy
    {
        PlayerName NextPick(Dictionary<PlayerName, PlayerSeasonStatLine> playerStatLines, Dictionary<SelectionCriteriaOptions, bool> selectionCritera);
    }
}
